# Ansible role template

[![Pipeline Status](https://git.intermax.nl/automation-development/configuration_management/ansible/roles/_template/badges/master/pipeline.svg)](https://git.intermax.nl/automation-development/configuration_management/ansible/roles/_template/pipelines)

Cookiecutter template for a Ansible role with Molecule testing.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)
```
    pip install -U cookiecutter
```
Generate a Python package project::
```
    cookiecutter https://gitlab.com/intermax-group/ansible/role-template.git
```

Or use molecule to generate the role:

```
    molecule init template --url https://gitlab.com/intermax-group/ansible/role-template.git
```
