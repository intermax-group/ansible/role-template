# {{cookiecutter.role_name}}

[![Pipeline Status](https://git.intermax.nl/automation-development/configuration_management/ansible/roles/{{cookiecutter.role_name}}/badges/master/pipeline.svg)](https://git.intermax.nl/automation-development/configuration_management/ansible/roles/{{cookiecutter.role_name}}/pipelines)

{{cookiecutter.description}}

## Requirements

None.

## Role Variables

### Default usage

{{cookiecutter.description}}

### Advanced usage

For more advanced usage the following variables are available:
```yaml
# Add default.yml values here
```

## Dependencies

None.

## Example Playbook

Install {{cookiecutter.role_name}}
```yaml
- hosts: all
  roles:
    - role: {{cookiecutter.role_name}}
```

After running the playbook ....

## License

GNU General Public License v3.0, see [LICENSE](LICENSE) for the full text.

## Author Information

This role was created in {{cookiecutter.year}} by @{{cookiecutter.ad_username}}
